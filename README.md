
craneweb - modern micro web framework for C
===========================================

	    _ 
	   /.)
	  ;|\|    modern
	  /)\`    micro
	 //)/     web
	//&&      framework
	' ||      for
	  ||      C
	  """
	          


(C) 2011-2014 Francesco Romani < fromani | gmail : com >

Overview
--------

craneweb is an up-to-date micro web framework for C, which tries to reproduce
the look and feel of the modern web frameworks.
craneweb tries heavy inspiration from the following frameworks:

* [Raphters](https://github.com/DanielWaterworth/Raphters)
* [bottle](https://github.com/defnull/bottle)

craneweb is licensed under the terms of the ZLIB license.
craneweb is meant to run on any POSIX-compliant platform,
but is developed and tested on Linux/amd64.

Usage scenario
--------------

Sometimes it is useful to embed a web interface into a daemon/server, for
debug, inspection of the state or for emergency procedures.
Craneweb aims to provide an easy-to-embed, easy-to-use, lightweight solution
for this specific task.
For this very reson, crane is a micro framework, not a full-stack one.

The data-layer component (e.g. ORM) is intentionally out of the scope
of the project: the state which the web interface has to expose is already
present inside the application itself, and the binding is necessarily
very application-dependent.

The template component itself, altough present and recommended,
is enterely optional.


Project directions
------------------

Given the intended usage scenario, the project directions which drive
the development of craneweb are:

* ease of embeddability.
* compactness: the bloat should be trimmed as most as is possible.
* zero-external dependencies: any external dependency is bundled with craneweb.
  Of course all licenses are compatible (and BSD-like). 
* simplicity: the code should be easy to read, easy to maintain and
  easy to keep in mind.


Requirements/Dependencies (common)
----------------------------------

* C89 compiler (tested against gcc 4+).
* [cmake](http://www.cmake.org)


Bundled dependencies (software craneweb relies on)
------------------------------------------------

* [msinttypes](http://code.google.com/p/msinttypes/)
* [mongoose](http://code.google.com/p/mongoose/)
* [http-parser](https://github.om/mojaves/http-parser) master repo [here](https://github.com/ry/http-parser)
* [libuseful](https://github.com/mojaves/lobuseful) master repo [here](https://github.com/breckinloggins/libuseful)
* [ngtemplate](https://github.com/mojaves/ngtemplate) master repo [here](https://github.com/breckinloggins/ngtemplate)
* [Spencer's regex](http://www.arglist.com/regex)


TODO
----

* {XML,JSON}RPC support (probably JSONRPC + discoverability)
* working forms example
* url{de,en}coding of the parsed query parameters 
* more docs
* lots more of tests
* amalgamation/easy embeddability


Getting Started
---------------

To build craneweb, you can use `cmake` as usual.
You will get the binary library(ies) and a few examples.


How to embed
------------

Craneweb is intended to be easily shipped with your project.
The license of craneweb iself is intentionally very liberal for this very purpose,
and the dependencies are selected to match this criteria. However, you may want to re-check
the licenses to see if the license of the craneweb stack you've built is compatible with
the license of your project.

In order to embed craneweb in your project, you have two routes.
The easy route is to build the craneweb library (either shared or static as you
like most) and bundle and/or ship as you like most, together with the craneweb.h
header file

You can also directly drop the craneweb sources on your project.
However, you should consider the following points:

* While craneweb has minimal external dependencies (it boils down to a decent C89 compiler
  and a reasonnably recent libc), the layout of the sources is significant and
  the craneweb source tree has to be replicated as subtree of your project.

* You can either adapt the given CMakeLists.txt or take them as reference and adapt
  your build system by borrowing the C compiler flags and linker options.


More Documentation
------------------

`src/craneweb.h` has extensive API documentation in doxygen format

TO WRITE...

### EOF ###


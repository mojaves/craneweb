/**************************************************************************
 * check_query_params: craneweb query parameters test suite.              *
 **************************************************************************/
#include <strings.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdio.h>

#include <check.h>

#include "config.h"

#include "craneweb.h" 
#include "craneweb_private.h" 


/*************************************************************************/

START_TEST(test_none)
{
    CRW_Parameters *P = CRW_parameters_new("");
    fail_if(P == NULL, "failed to allocate a parameters object");
    CRW_parameters_del(P);
}
END_TEST

struct pair {
    const char *K;
    const char *V;
};

static void test_query_string(CRW_Parameters *P, int N, const struct pair *KV)
{
    int i = 0, num = 0;
    fail_if(!P, "parameters init failed");
    num = CRW_parameters_count_items(P);
    fail_unless(num == N, "item number mismatch");
    for (i = 0; KV[i].K; i++) {
        const char *val = NULL;
        if (KV[i].V) {
            val = CRW_parameters_get_item_by_name(P, KV[i].K);
            fail_unless(val && !strcmp(val, KV[i].V), "item value not found");
        } else {
            val = CRW_parameters_get_item_by_name(P, KV[i].K);
            fail_unless(!val, "unexpected item found");
        }
    }
    return;
}

static int N_from(const struct pair *KV)
{
    int i = 0, N = 0;
    for(i = 0; KV[i].K; i++) {
        N += !!(KV[i].K && KV[i].V);
        /* whoah, we're dealing with a badass here */
    }
    return N;
}

static void build_and_test(const char *QS, const struct pair *KV)
{
    CRW_Parameters *P = CRW_parameters_new(QS);
    test_query_string(P, N_from(KV), KV);
    CRW_parameters_del(P);
}



START_TEST(test_querystring_simple1)
{
    struct pair KV[] = {{"k0",NULL}, {"k1","v1"}, {NULL, NULL}};
    const char *QS = "k1=v1";
    build_and_test(QS, KV);
}
END_TEST

START_TEST(test_querystring_simple_dirty1a)
{
    struct pair KV[] = {{"k0",NULL}, {"k1","v1"}, {NULL, NULL}};
    const char *QS = "&k1=v1";
    build_and_test(QS, KV);
}
END_TEST

START_TEST(test_querystring_simple_dirty1b)
{
    struct pair KV[] = {{"k0",NULL}, {"k1","v1"}, {NULL, NULL}};
    const char *QS = "k1=v1&";
    build_and_test(QS, KV);
}
END_TEST

START_TEST(test_querystring_simple2)
{
    struct pair KV[] = {{"k1","v1"}, {"k2","v2"}, {NULL, NULL}};
    const char *QS = "k1=v1&k2=v2";
    build_and_test(QS, KV);
}
END_TEST

START_TEST(test_querystring_simple_dirty2a)
{
    struct pair KV[] = {{"k1","v1"}, {"k2","v2"}, {NULL, NULL}};
    const char *QS = "&k1=v1&k2=v2";
    build_and_test(QS, KV);
}
END_TEST

START_TEST(test_querystring_simple_dirty2b)
{
    struct pair KV[] = {{"k1","v1"}, {"k2","v2"}, {NULL, NULL}};
    const char *QS = "k1=v1&k2=v2&";
    build_and_test(QS, KV);
}
END_TEST

START_TEST(test_querystring_simple3)
{
    struct pair KV[] = {{"k1","v1"}, {"k2","v2"}, {"k3","v3"}, {NULL, NULL}};
    const char *QS = "k1=v1&k2=v2&k3=v3";
    build_and_test(QS, KV);
}
END_TEST

START_TEST(test_querystring_novalue1)
{
    struct pair KV[] = {{"k0",NULL}, {"k1","k1"}, {NULL, NULL}};
    const char *QS = "k1";
    build_and_test(QS, KV);
}
END_TEST

START_TEST(test_querystring_novalue2a)
{
    struct pair KV[] = {{"k1","k1"}, {"k2","v2"}, {NULL, NULL}};
    const char *QS = "k1&k2=v2";
    build_and_test(QS, KV);
}
END_TEST

START_TEST(test_querystring_novalue2b)
{
    struct pair KV[] = {{"k1","v1"}, {"k2","k2"}, {NULL, NULL}};
    const char *QS = "k1=v1&k2";
    build_and_test(QS, KV);
}
END_TEST

START_TEST(test_querystring_novalue2_seq)
{
    struct pair KV[] = {{"k1","k1"}, {"k2","k2"}, {NULL, NULL}};
    const char *QS = "k1&k2";
    build_and_test(QS, KV);
}
END_TEST

START_TEST(test_querystring_novalue3_seq)
{
    struct pair KV[] = {{"k1","k1"}, {"k2","k2"}, {"k3", "k3"}, {NULL, NULL}};
    const char *QS = "k1&k2&k3";
    build_and_test(QS, KV);
}
END_TEST

START_TEST(test_querystring_novalue1_dirtya)
{
    struct pair KV[] = {{"k0",NULL}, {"k1","k1"}, {NULL, NULL}};
    const char *QS = "&k1";
    build_and_test(QS, KV);
}
END_TEST

START_TEST(test_querystring_novalue1_dirtyb)
{
    struct pair KV[] = {{"k0",NULL}, {"k1","k1"}, {NULL, NULL}};
    const char *QS = "k1&";
    build_and_test(QS, KV);
}
END_TEST

START_TEST(test_querystring_novalue1_dirtyc)
{
    struct pair KV[] = {{"k0",NULL}, {"k1","k1"}, {NULL, NULL}};
    const char *QS = "&k1&";
    build_and_test(QS, KV);
}
END_TEST





/*************************************************************************/

TCase *craneweb_testCaseQueryParams(void)
{
    TCase *tcParams = tcase_create("craneweb.core.query.parameters");
    tcase_add_test(tcParams, test_none);
	tcase_add_test(tcParams, test_querystring_simple1);
	tcase_add_test(tcParams, test_querystring_simple_dirty1a);
	tcase_add_test(tcParams, test_querystring_simple_dirty1b);
	tcase_add_test(tcParams, test_querystring_simple2);
	tcase_add_test(tcParams, test_querystring_simple_dirty2a);
	tcase_add_test(tcParams, test_querystring_simple_dirty2b);
	tcase_add_test(tcParams, test_querystring_simple3);
	tcase_add_test(tcParams, test_querystring_novalue1);
	tcase_add_test(tcParams, test_querystring_novalue2a);
	tcase_add_test(tcParams, test_querystring_novalue2b);
    tcase_add_test(tcParams, test_querystring_novalue2_seq);
    tcase_add_test(tcParams, test_querystring_novalue3_seq);
    tcase_add_test(tcParams, test_querystring_novalue1_dirtya);
    tcase_add_test(tcParams, test_querystring_novalue1_dirtyb);
    tcase_add_test(tcParams, test_querystring_novalue1_dirtyc);
    return tcParams;
}

static Suite *craneweb_suiteParams(void)
{
    TCase *tc = craneweb_testCaseQueryParams();
    Suite *s = suite_create("craneweb.core.query.parameters");
    suite_add_tcase(s, tc);
    return s;
}

/*************************************************************************/

int main(int argc, char *argv[])
{
    int number_failed = 0;

    Suite *s = craneweb_suiteParams();
    SRunner *sr = srunner_create(s);

    srunner_run_all(sr, CK_ENV);
    number_failed = srunner_ntests_failed(sr);
    srunner_free(sr);
    return (number_failed == 0) ? EXIT_SUCCESS : EXIT_FAILURE;
}

/*************************************************************************/

/* vim: set ts=4 sw=4 et */
/* EOF */

